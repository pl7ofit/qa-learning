# Qa Learning
---

## #1 Изучение git
<details>

* Заполнить профиль gitlab
* Установить git
* Установить openssh-client
* Выпустить себе ssh ключ и вписать публичный ключ в профиль gitlab
* Склонировать этот репозиторий через git clone ssh...
* [готово] - вписать тут слово "готово" (через запрос на слияние)
* Создать "запрос на слияние" с обновлением этого файла
* **(дополнительно)** Описать ниже какими командами я обновил этот файл
```
git status
git add .
git commit
git push
```
</details>

---

## #2 Определение языков программирования и разметки

<details>

* Ниже будут отрывки из разных **языков программирования**,** языков разметки** и  **языков обмена данными**, задача - прописать вверху название языка и сделать MR с ответами.
* Список языков ниже:
	- java+
	- c+
	- json+
	- sql+
	- bash+
	- html+
	- asm+
	- xml+
	- python+
	- csv+
	- lisp+
	- yaml+
	- javascript+
	- markdown+
	- golang+
	- ini+
	- regex (perl)+
---

**1.** c
```
#include <stdio.h>
#define MAX2(a,b) (((a)>(b))?(a):(b))

int main(void) {
  printf("%i\n", MAX2(2,1));
}
```
---
**2.** python
```
import sys, os

max2 = lambda a,b: a or b

def main():
  print(max2(2,1))
main()
```
---
**3.** bash
```
source script.sh
function max2()
{
  echo [[ ${1} || ${2} ]] && echo ${1} || echo {2}
}

echo $(max2 2 1)
```
---
**4.** html
```    
<html>
 <head>
  <title>header</title>
  <meta charset="utf-8">
 </head>
 <body>
  <header>
   <img src="/example/image/logo.png" alt="Logo"
   width="100" height="100">
   <h1>Wellcome!</h1>
  </header>
  <main>
    Content
  </main>
 </body>
</html>
```
---
**5.** json
```
{
  "fruits":["apple", "orange", "pear"],
  "pets":["cat", "dog", "bird"],
  "planet": "earth",
  "start":{
             "active": true,
             "name": "Sun"
          }
}

```

---
**6.** lisp
```
(format t "Hello, World!~%")
```
---
**7.** yaml
```
country:
  france:
    cars:
      - renault
      - peugeot
      - bugatti
    is_eu: true
```
---
**8.** xml
```
<?xml version="1.0" encoding="UTF-8"?>
- <note>
  <to>Tove</to>
  <from>Jani</from>
  <heading>Reminder</heading>
  <body>Don't forget me this weekend!</body>
</note>
```
---
**9.** ini
```
[kaliningrad]
; this is comment
vapeshops=voshod, parovoz
supermarkets=victoria, spar, metro
```
---
**10.** asm
```
use16
org 100h
 
    mov dx,hello
    mov ah,9
    int 21h
 
    mov ax,4C00h
    int 21h
hello db 'Hello, world!$'
```
---
**11.** sql
```
WITH patient_data AS (
    SELECT patient_id, patient_name, hospital, drug_dosage
    FROM hospital_registry
    WHERE (last_visit &gt; now() - interval '14 days' OR last_visit IS NULL)
    AND city = "Los Angeles"
)
 
WITH average_dosage AS (
    SELECT hospital, AVG(drug_dosage) AS Average
    FROM patient_data
    GROUP BY hospital
)
```
---
**12.** javascript
```
document.write('Hello, World!');
```
---
**13.** regex
```
(^(?P<>H[^ ]*) (?P<>W[^ ]*\!)$)
```
---
**14.** golang
```
package main
import "fmt"
func main() {
    fmt.Println("hello world")
}
```
---
**15.** java
```
class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
```
---
**16.** csv
```
1965;Пиксель;E240 – формальдегид (опасный консервант)!;"красный, зелёный, битый";"3000,00"
1965;Мышка;"А правильнее использовать ""Ёлочки""";;"4900,00"
"Н/д";Кнопка;Сочетания клавиш;"MUST USE! Ctrl, Alt, Shift";"4799,00
```

**17.** markdown
```
В каком формате сам файл README.md?
```
</details>


## #3 Базовое знакомство с некоторыми ЯП


<details>

* написать любой код на python в файл lesson3/code.py
* написать любой код на C в файл lesson3/code.c
* написать все типы данных в формате json в lesson3/code.json
* написать все типы данных в формате yaml в lesson3/code.yaml
* написать что-то в формате xml в lesson3/code.xml
* написать код на bash с использованием grep и regex в файле lesson3/code.sh
* все тесты должны быть пройдены
* сделать МР в мастер
</details>

## #4 Быстропечатанье английских слов и не только


<details>

* Пройти регистрацию на стайте https://stamina-online.com для сохранения прогресса
* Раздел английской раскладки https://stamina-online.com/ru/lessons/en
* Тоочность должна быть не ниже 85%
* Нужно пройти три любых тренажера из раздела **Базовые уроки** на **английской раскладке** со **скоростью печати не менее 115 символов в минуту**
* Нужно пройти три тренажера **Short words** **Long words** и **Capital letters** из раздела **Набираем слова** на **английской раскладке** со **скоростью печати не менее 140 символов в минуту**
* Нужно пройти **All together** из раздела **Знаки препинания** на **английской раскладке** со **скоростью печати не менее 110 символов в минуту**
* Нужно пройти **All together** из раздела **Цифры и символы** на **английской раскладке** со **скоростью печати не менее 110 символов в минуту**
* Нужно пройти **All together** из раздела **Остальные символы** на **английской раскладке** со **скоростью печати не менее 110 символов в минуту**
* Результаты прикрепить ссылкой(если там такое есть)
</details>


## #5 Практическое задание по программированию на скриптовом ЯП Python


<details>

* Написать текстовую игру "Змейка"
* Необходимо рисовать поле состоящее как минимум из 16 символов по шерине и высоте, необходимо использовать массив **list**
* Необходимо использовать несколько вложенных циков **for**
* Для обозначения пустой клетки поля можно использовать пробел " " или ноль "0"
* Для обознечения не пустой клетки можно использовать "#" или "1"
* Края поля должны быть зацикленными
* Змейка должны бать массивом **list** который содержит координаты точек змейки на поле
* Тело змейки двигается за её головой, сдвигая все координаты клеток змейки вперёд по массиву
* Управление змейкой, еду, очки и препятствие для змейки придумаем в следующем задании. Пока пусть змейка последовательно каждые 4 хода сама ходит вверх, вправо, вниз, влево и так по кругу.
</details>
